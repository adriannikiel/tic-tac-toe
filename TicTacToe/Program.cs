﻿using System;

namespace TicTacToe
{
    class Program
    {
        static Random random = new Random();

        static char[,] grid = new char[3, 3] {
         { '_', '_', '_'},
         { '_', '_', '_'},
         { '_', '_', '_'}
        };

        static void Main(string[] args)
        {

            bool isPlayerMove = true;

            while (true)
            {
                if (isPlayerMove)
                {
                    PlayerMove();
                    isPlayerMove = false;
                }
                else
                {
                    ComputerMove();
                    isPlayerMove = true;
                }

                State result = AnalyzeGrid();

                switch (result)
                {
                    case State.PlayerWin:
                        {
                            PrintGrid();
                            Console.WriteLine("\nPlayer win!");
                            Console.ReadLine();
                            return;
                        }
                    case State.ComputerWin:
                        {
                            PrintGrid();
                            Console.WriteLine("\nComputer win!");
                            Console.ReadLine();
                            return;
                        }
                    case State.Draw:
                        {
                            PrintGrid();
                            Console.WriteLine("\nDraw!");
                            Console.ReadLine();
                            return;
                        }
                    case State.Impossible:
                        {
                            PrintGrid();
                            Console.WriteLine("\nSomething is wrong!");
                            Console.ReadLine();
                            return;
                        }
                    default: break;
                }
            }
        }


        private static void PlayerMove()
        {
            PrintGrid();
            Console.WriteLine("___________________");

            while (true)
            {
                Console.Write("Please choose row and column: ");

                string playerField = Console.ReadLine();
                string[] splittedField = playerField.Split(' ');

                int row = int.Parse(splittedField[0]);
                int column = int.Parse(splittedField[1]);

                char letter = grid[row - 1, column - 1];

                if (letter == '_')
                {
                    grid[row - 1, column - 1] = 'X';
                    break;
                }
                else
                {
                    Console.WriteLine("Field is not empty. Try again! \n");
                }
            }
        }


        private static void ComputerMove()
        {
            PrintGrid();

            Console.WriteLine("___________________");
            Console.WriteLine("Computer move:");

            while (true)
            {
                int row = random.Next(0, 3);
                int column = random.Next(0, 3);

                char letter = grid[row, column];

                if (letter == '_')
                {
                    grid[row, column] = 'O';
                    break;
                }
            }
        }

        private static State AnalyzeGrid()
        {
            if (checkImppossible())
            {
                return State.Impossible;
            }

            if (isWinner('X'))
            {
                return State.PlayerWin;
            }

            if (isWinner('O'))
            {
                return State.ComputerWin;
            }

            if (countLetterInGrid('X') + countLetterInGrid('O') == 9)
            {
                return State.Draw;
            }

            return State.NotFinished;
        }

        private static bool checkImppossible()
        {
            int countX = countLetterInGrid('X');
            int countY = countLetterInGrid('O');

            if (Math.Abs(countX - countY) > 1)
            {
                return true;
            }

            bool isWinnerX = isWinner('X');
            bool isWinnerY = isWinner('O');

            return isWinnerX && isWinnerY;
        }

        private static bool isWinner(char letter)
        {
            int count = 0;

            //check rows
            for (int i = 0; i < grid.GetLength(0); i++)
            {
                for (int j = 0; j < grid.GetLength(1); j++)
                {
                    if (grid[i, j] == letter)
                    {
                        count++;
                    }
                }

                if (count == 3)
                {
                    return true;
                }

                count = 0;
            }

            // check columns
            for (int i = 0; i < grid.GetLength(0); i++)
            {
                for (int j = 0; j < grid.GetLength(1); j++)
                {
                    if (grid[j, i] == letter)
                    {
                        count++;
                    }
                }

                if (count == 3)
                {
                    return true;
                }

                count = 0;
            }

            // check diagonals
            if (grid[0, 0] == letter && grid[1, 1] == letter && grid[2, 2] == letter)
            {
                return true;
            }

            if (grid[0, 2] == letter && grid[1, 1] == letter && grid[2, 0] == letter)
            {
                return true;
            }

            return false;
        }

        private static int countLetterInGrid(Char letter)
        {
            int result = 0;

            for (int i = 0; i < grid.GetLength(0); i++)
            {
                for (int j = 0; j < grid.GetLength(1); j++)
                {
                    if (letter == grid[i, j])
                    {
                        result++;
                    }
                }
            }

            return result;
        }

        private static void PrintGrid()
        {
            for (int i = 0; i < grid.GetLength(0); i++)
            {
                for (int j = 0; j < grid.GetLength(1); j++)
                {
                    Console.Write("{0} ", grid[i, j]);
                }
                Console.WriteLine();
            }
        }
    }

    enum State
    {
        PlayerWin, ComputerWin, Draw, Impossible, NotFinished
    }
}
