# Tic Tac Toe

Simple game Tic Tac Toe.

## Description (from wiki)

Tic-tac-toe (American English), noughts and crosses (Commonwealth English), or Xs and Os (Canadian or Irish English) is a paper-and-pencil game for two players who take turns marking the spaces in a three-by-three grid with X or O. The player who succeeds in placing three of their marks in a horizontal, vertical, or diagonal row is the winner. It is a solved game, with a forced draw assuming best play from both players.

## Short video

https://www.youtube.com/watch?v=1L7H84IfNqE